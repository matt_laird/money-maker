# Money Maker

## Problem
Create an idle game inspired by [Adventure Capitalist](http://en.gameslol.net/adventure-capitalist-1086.html).

## Solution
Created a GUI web game with vanilla JS (no frameworks) on the front-end and [Google Firebase](https://firebase.google.com/) for running a serverless mobile backend.

## Play the Demo

[Web Link](https://money-maker-81e6c.web.app/)

## Core Features

- Buy new shops
- Start running a shop to earn money at a regular interval
- Manage a shop to automate its payout
- Upgrade a shop to increase its payout
- Offline earnings - shops will continue to payout while you are away
- "Free Money" - cheat the game and receive $10,000,000
- "Reset Game" - reset all progress and start the game over

## Additional Features

- [Hosting](https://firebase.google.com/products/hosting/)
   -- Fast, secure, reliable web hosting
- [Anonymous Auth](https://firebase.google.com/products/auth/)
   -- Validate the user and assign an id by browser IP
- [Firestore](https://firebase.google.com/products/firestore/)
   -- Store each player's progress remotely in a NoSQL format
   -- Save progress in the cache while offline and upload data when internet connection returns
- [Analytics](https://firebase.google.com/products/analytics/)
   -- Save and sync data between Firebase and Google Analytics
   -- Tag and sort users by demographics and view trends in session times, spending habits, etc.


## Pending Features (Wish List)

These features were not implemented due to time restraints. They are considered necessary for a secure and robust game that could be released into production.

- Create flexible UI that adjusts to different screen sizes
- Change Firestore database from test mode to production mode
- Create strict security rules for database access
- Restrict user read/write access to his/her data only
- Allow user to login with a third-party account (Facebook, Google, email, etc.)
- Move game data (store prices, payout, etc.) completely to the server
- Add remote config for A/B testing game data (store prices, payout, etc)
- Send push notifications to users when outside the game