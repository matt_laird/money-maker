/* SERVER LOGIC - read / write access of user data to the backend */

// Is the user allowed to write to the server database?
let can_write = true;

// Wait for the document to load before calling any logic
document.addEventListener('DOMContentLoaded', function() {
    // The Firebase SDK is initialized and available here!
    firebase.analytics();

    // Sign in the user anonymously
    firebase.auth().signInAnonymously().catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage);
    });

    // Has the user logged in or out?
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            // User is signed in.
            user_id = user.uid;
            console.log("user " + user_id);

            // Get user data from the backend
            getUserData();
        } else {
            // User is signed out - show a message
            document.getElementById("bank_balance_label").innerHTML = "Offline. Try again.";
        }
    });

    // Get a reference to the Firebase app
    try {
        let app = firebase.app();
    } catch (e) {
        console.error(e);
    }
});


// Get user data
function getUserData() {

    // Get the file for the user id
    firebase.firestore().collection("users").doc(user_id).get().then(function(doc) {
        // Does the file exist?
        if (doc.exists) {
            // Set the local data from remote data
            console.log("Document data:", doc.data());
            var user_data = doc.data();
            bank_balance = user_data.bank_balance;
            inventory_list = user_data.inventory;
            // Start playing the game
            startGame();
        } else {
            // Is the file missing? If so, this is a new user
            // doc.data() will be undefined in this case
            console.log("No such document!");
            // Start playing the game - use the local default data
            startGame();
        }
    }).catch(function(error) {
        // There was an access error - ask the user to refresh
        console.log("Error getting document:", error);
        document.getElementById("bank_balance_label").innerHTML = "Offline. Try again.";
    });
}

// Set user data
function setUserData() {

    // Can the user write data? False if they have deleted their data
    if (can_write) {

        // Bulk upload
        var userData = {
            user_id: user_id,
            bank_balance: bank_balance,
            inventory: inventory_list
        };

        // Update user data
        firebase.firestore().collection("users").doc(user_id).set(userData)
            .then(function() {
                // Success
                console.log("Document successfully written!");
            })
            .catch(function(error) {
                // Error
                console.error("Error writing document: ", error);
            });
    }
}

// Remove the user data completely
function deleteUserData() {
    // Set the flag to false so the user cannot update data - stops async methods
    can_write = false;
    firebase.firestore().collection("users").doc(user_id).delete().then(function() {
        // Success
        console.log("Document successfully deleted!");
        // Reload the window - game will restart
        location.reload();
    }).catch(function(error) {
        // Error
        console.error("Error removing document: ", error);
    });
}