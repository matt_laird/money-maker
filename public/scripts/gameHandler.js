/* GAME LOGIC - actions taken by the user */

// Call this method after page load and user is authenticated
function startGame() {
    updateBalance();

    createShops();

    createExtraButtons();

    refreshShops();

    progressShops();

}

// Create views all shops
function createShops() {
    for (const shop of shop_list) {
        createShop(shop);
    }
}

// Refresh views for all shops
function refreshShops() {
    for (const shop of shop_list) {
        refreshShop(shop);
    }

    setUserData();
}

// Refresh progress bars for all shops
// Run at every 1/10 second interval
function progressShops() {
    setInterval(function() {
        for (const shop of shop_list) {
            progressShop(shop);
        }
    }, 100);
}

// Give the user free money
function cheatGame() {
    addBalance(10000000);
    refreshShops();
}

// Reset the user and start the game over
function resetGame() {
    deleteUserData();
}

// Return a number in money format
function formatMoney(number) {
    // example $1,000,000
    return "$" + number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

// Return a number in clock format: mm:ss: 99:99
function formatClock(shop, item) {

    // 
    let time_difference = shop.time_frame;

    // check if the shop timer has already started
    if (item.time_stamp != 0) {
        // get current timestamp
        let time_now = Date.now();
        // get the elapsed time between now and when the shop cycle started
        time_difference = shop.time_frame - (time_now - item.time_stamp);
    }

    //let hours = parseInt((time_difference/(1000*60*60))%24);
    let minutes = parseInt((time_difference / (1000 * 60)) % 60);
    let seconds = parseInt((time_difference / 1000) % 60);
    let milliseconds = parseInt((time_difference % 1000) / 10)

    // add a 0 to string if digit is less than 10
    //hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;
    milliseconds = (milliseconds < 10) ? "00" : milliseconds;

    // example 03:42.27
    return minutes + ":" + seconds + ":" + milliseconds;
}

// Check if the amount is less than or equal to the user's bank balance
function checkBalance(amount) {
    if (bank_balance >= amount) {
        return true
    }

    return false
}

// Refresh the visible bank balance label
function refreshBalance() {
    let balance = formatMoney(bank_balance);
    document.getElementById("bank_balance_label").innerHTML = balance;
}

// Calculate the amount of money user has earned while outside the game
function updateBalance() {

    // Default is 0
    let income_amount = 0;

    // Loop through each shop
    for (const shop of shop_list) {
        // Get the user inventory item
        let item = inventory_list[shop.id];

        // Has the shop cycle been started?
        if (item.time_stamp > 0) {
            // Calculate the elapsed time in the cycle
            let time_now = Date.now();
            let time_difference = time_now - item.time_stamp;
            // Has the cycle finished?
            if (time_difference >= shop.time_frame) {
            // Calculate the payout
                let amount = getRevenueAmount(shop, item);
                // Is the shop managed?
                if (item.is_managed) {
                    // Get the number of completed cycles. It could be > 1.
                    let multiplyer = Math.floor(time_difference / shop.time_frame);
                    // Payout the revenue in multiples
                    amount *= multiplyer;
                    // Calculate and set the remaining time period of the current cycle
                    let time_left = time_difference % shop.time_frame;
                    item.time_stamp = Date.now() - time_left;
                } else {
                    // If the shop is not managed, reset its timer
                    item.time_stamp = 0;
                }
                // Add this shop's income to the total income
                income_amount += amount;
            }
        }
    }

    // Have any of the shops earned income this cycle?
    if (income_amount > 0) {
        // Add the total income to the user's bank balance
        bank_balance += income_amount;
        // Alert the user to the total income they earned outside the game
        window.alert("You earned " + formatMoney(income_amount) + " while you were away!");
    }

    // Refresh the balance label
    refreshBalance();
}

// Spend money. Remove it from the bank balance.
function subtractBalance(amount) {
    // Does the user have enough money in their bank balance?
    if (checkBalance(amount)) {
        // Make the change and refresh the label
        bank_balance -= amount;
        refreshBalance();
    }
}

// Earn money. Add it to the bank balance.
function addBalance(amount) {
    // Make the change and refresh the label
    bank_balance += amount;
    refreshBalance();
}

// Add 2 extra buttons (only for test mode)
function createExtraButtons() {

    let row = document.createElement("div");
    row.setAttribute("style", "margin:64px");
    row.align = "center";
    document.body.appendChild(row);

    let cheat_button = document.createElement("button");
    cheat_button.innerHTML = "FREE MONEY";
    cheat_button.setAttribute("class", "w3-button w3-small w3-purple w3-round-large");
    cheat_button.setAttribute("style", "width:192px;margin:20px");
    cheat_button.addEventListener("click", cheatGame);
    row.appendChild(cheat_button);

    let reset_button = document.createElement("button");
    reset_button.innerHTML = "RESET GAME";
    reset_button.setAttribute("class", "w3-button w3-small w3-red w3-round-large");
    reset_button.setAttribute("style", "width:192px;margin:20px");
    reset_button.addEventListener("click", resetGame);
    row.appendChild(reset_button);
}

// Create the UI for each shop
function createShop(shop) {

    // Make a row
    let item = inventory_list[shop.id];
    let row = document.createElement("div");
    row.setAttribute("style", "margin:12px");
    row.align = "center";
    document.body.appendChild(row);

// Make an icon
    let icon = document.createElement("img");
    icon.src = "/images/" + shop.image + ".png";
    icon.setAttribute("height", "32px");
    row.appendChild(icon);

// Make a title label
    let title_label = document.createElement("span");
    title_label.setAttribute("class", "w3-small");
    title_label.setAttribute("style", "display: inline-block;width:128px");
    title_label.innerHTML = shop.title;
    row.appendChild(title_label);

// Make a Start button
    let start_button = document.createElement("button");
    start_button.id = "start_button_" + shop.id;
    start_button.innerHTML = "START";
    start_button.setAttribute("class", "w3-button w3-small w3-black w3-round-large");
    start_button.setAttribute("style", "width:192px;");
    start_button.addEventListener("click", function() {
        document.getElementById("start_sound").play();
        startShop(shop);
    });
    row.appendChild(start_button);

// Make the background to the progress bar
    let outerBar = document.createElement("div");
    outerBar.id = "progress_bar_outer_" + shop.id;
    outerBar.setAttribute("class", "w3-light-grey w3-round-xlarge");
    outerBar.setAttribute("style", "width:192px;height:32px;display: inline-block;vertical-align: middle;");
    row.appendChild(outerBar);

// Make the flexible inner indicator to the progress bar
    let innerBar = document.createElement("div");
    innerBar.id = "progress_bar_inner_" + shop.id;
    innerBar.setAttribute("class", "w3-orange w3-round-xlarge");
    innerBar.setAttribute("style", "width:0%;height:100%;float:left;vertical-align: middle;");
    outerBar.appendChild(innerBar);

// Make a countdown timer for the shop cycle
    let countdown_label = document.createElement("span");
    countdown_label.id = "countdown_label_" + shop.id;
    countdown_label.setAttribute("class", "w3-small");
    countdown_label.setAttribute("style", "display: inline-block;width:192px;height:100%;padding-top:8px");
    innerBar.appendChild(countdown_label);

// Make a tally label to indicate the shop revenue per cycle
    let level_label = document.createElement("span");
    level_label.id = "level_label_" + shop.id;
    level_label.setAttribute("class", "w3-small");
    level_label.setAttribute("style", "display: inline-block;width:128px;text-align:left;padding-left:12px");
    row.appendChild(level_label);

// Make an upgrade button for user to buy and upgrade the shop
    let upgrade_button = document.createElement("button");
    upgrade_button.id = "upgrade_button_" + shop.id;
    upgrade_button.innerHTML = "UPGRADE";
    upgrade_button.setAttribute("class", "w3-button w3-small w3-green w3-round-large");
    upgrade_button.setAttribute("style", "display: inline-block;width:128px;");
    upgrade_button.addEventListener("click", function() {
        upgradeShop(shop);
    });
    row.appendChild(upgrade_button);

// Make a price label for upgrading
    let upgrade_label = document.createElement("span");
    upgrade_label.id = "upgrade_label_" + shop.id;
    upgrade_label.setAttribute("class", "w3-small");
    upgrade_label.setAttribute("style", "display: inline-block;width:128px;text-align:left;padding-left:12px");
    row.appendChild(upgrade_label);

// Make a manage button for the user to buy a manager
    let manage_button = document.createElement("button");
    manage_button.id = "manage_button_" + shop.id;
    manage_button.innerHTML = "MANAGE";
    manage_button.setAttribute("class", "w3-button w3-small w3-blue w3-round-large");
    manage_button.addEventListener("click", function() {
        manageShop(shop);
    });
    row.appendChild(manage_button);

// Make a price label for managing
    let manage_label = document.createElement("span");
    manage_label.id = "manage_label_" + shop.id;
    manage_label.setAttribute("class", "w3-small");
    manage_label.setAttribute("style", "display: inline-block;width:128px;text-align:left;padding-left:12px");
    row.appendChild(manage_label);
}

// Start a new payout cycle for this shop
function startShop(shop) {
    let item = inventory_list[shop.id];
    if (item.time_stamp == 0) {
        // Set the current time for this cycle
        item.time_stamp = Date.now();
        // Show progress
        progressShop(shop);
        // Set user data
        setUserData();
    }
}

// Stop the payout cycle for this shop
function stopShop(shop) {
    let item = inventory_list[shop.id];
    // Add the revenue for this cycle
    let amount = getRevenueAmount(shop, item);
    addBalance(amount);
    // Update the UI
    refreshShops();
}

// Calculate the revenue payout for this shop
function getRevenueAmount(shop, item) {
    // Multiply the single shop revenue X the number of identical shops owned
    return shop.revenue * item.level;
}

// Calculate the cost of upgrading this shop
function getUpgradeAmount(shop, item) {
    // Formula: shop payout + (shop payout X number of shops squared)
    // Cost of each upgrade doubles per level
    return shop.revenue + (shop.revenue * item.level * item.level);
}

// Calculate the cost of managing this shop
function getManageAmount(shop) {
    // Formula: shop payout X 100
    return shop.revenue * 100;
}

// Uprade the payout for this shop
function upgradeShop(shop) {
    let item = inventory_list[shop.id];
    let upgrade_amount = getUpgradeAmount(shop, item);
    // Does the user have enough money to upgrade?
    if (checkBalance(upgrade_amount)) {
        // Spend the money
        subtractBalance(upgrade_amount);
        // Add another shop to the inventory
        item.level += 1;
        // Play a sound
        document.getElementById("upgrade_sound").play();
        // Refresh the UI
        refreshShops();
    }
}

// Buy a manager for this shop
function manageShop(shop) {
    let item = inventory_list[shop.id];
    // Make sure the shop is not already managed
    if (item.is_managed == false) {
        // Check if the user has enough money to manage
        if (checkBalance(getManageAmount(shop))) {
            // Spend the money
            subtractBalance(getManageAmount(shop));
            // Set the manager for the shop
            item.is_managed = true;
            // Play a sound
            document.getElementById("manage_sound").play();
            // Refresh the UI
            refreshShops();
            // Start the payout cycle for this shop
            startShop(shop);
        }
    }
}

// Update the UI for this sohp
function refreshShop(shop) {
    let item = inventory_list[shop.id];
    let level_label = document.getElementById("level_label_" + shop.id);
    let level_amount = getRevenueAmount(shop, item);
    level_label.innerHTML = formatMoney(level_amount);
    let countdown_label = document.getElementById("countdown_label_" + shop.id);
    let upgrade_label = document.getElementById("upgrade_label_" + shop.id);
    let upgrade_amount = getUpgradeAmount(shop, item);
    upgrade_label.innerHTML = formatMoney(upgrade_amount);
    let upgrade_button = document.getElementById("upgrade_button_" + shop.id);
    let manage_amount = getManageAmount(shop);
    let manage_label = document.getElementById("manage_label_" + shop.id);
    manage_label.innerHTML = formatMoney(manage_amount);
    let manage_button = document.getElementById("manage_button_" + shop.id);

    if (item.level > 0) {
            // If the shop is owned, show its level, countdown timer, and upgrade button
        level_label.style.visibility = "visible";
        countdown_label.style.visibility = "visible";
                upgrade_button.innerHTML = "UPGRADE";
    } else {
        // If the shop is not owned, hide its level, countdown timer, and upgrade button
        level_label.style.visibility = "hidden";
        countdown_label.style.visibility = "hidden";
        upgrade_button.innerHTML = "BUY";

    }

    if (checkBalance(upgrade_amount)) {
        // If the user has enough money to upgrade, enable the button
        upgrade_button.disabled = false;
    } else {
        // If the user does not have enough money to upgrade, disable the button
        upgrade_button.disabled = true;
    }

    if (item.is_managed == true || item.level == 0) {
        // If the shop is already managed, or is not owned, hide the manage UI
        manage_label.style.visibility = "hidden";
        manage_button.style.visibility = "hidden";
    } else {
        // Otherwise, show the manage UI
        manage_label.style.visibility = "visible";
        manage_button.style.visibility = "visible";
        if (checkBalance(manage_amount)) {
            // If the user has enough money to manage this shop, enable the button
            manage_button.disabled = false;
        } else {
            // Otherwise, disable the button
            manage_button.disabled = true;
        }
    }
}

// Update the progress UI for this shop
function progressShop(shop) {
    let item = inventory_list[shop.id];
    let start_button = document.getElementById("start_button_" + shop.id);
    let progress_bar_outer = document.getElementById("progress_bar_outer_" + shop.id);
    let progress_bar_inner = document.getElementById("progress_bar_inner_" + shop.id);
    let countdown_label = document.getElementById("countdown_label_" + shop.id);

    // Update the countdown clock
    countdown_label.innerHTML = formatClock(shop, item);

    // Has the current shop cycle started?
    if (item.time_stamp > 0) {
        // Calculate the elapsed time of the current cycle
        let time_now = Date.now();
        let time_difference = time_now - item.time_stamp;
        // Has the current shop cyle completed?
        if (time_difference >= shop.time_frame) {
            // Reset the progress bar and time stamp
            progress_bar_inner.style.width = "0%";
            item.time_stamp = 0;
            // If the shop is not managed, hide the progress bar and show the Start button
            if (item.is_managed == false) {
                progress_bar_outer.style.display = "none";
                start_button.style.display = "inline-block";
            }
            // Stop the shop progress
            stopShop(shop);
        } else {
            // The current shop cycle has not completed
            // Calculate the progress as a %
            // Show the flexible progress bar
            // Hide the Start button
            progress = time_difference / shop.time_frame;
            progress_bar_inner.style.width = progress * 100 + "%";
            progress_bar_outer.style.display = "inline-block";
            start_button.style.display = "none";
        }
    } else {
        if (item.is_managed) {
            // If the shop is managed, and the current cycle is complete, automatically start a new cycle
            startShop(shop);
        } else {
            // If the shop is not managed, hide the progress bar
            progress_bar_outer.style.display = "none";
            if (item.level > 0) {
                // If the shop is owned, show the Start button
                start_button.style.visibility = "visible";
            } else {
                // Otherwise hide the Start button
                start_button.style.visibility = "hidden";
            }
        }
    }
}