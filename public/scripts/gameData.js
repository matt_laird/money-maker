/* GAME DATA - variables only, no logic */

// Keep track of the user's money
let bank_balance = 0;

// Keep track of the user's id
var user_id = "0";


// These are all the shops available to buy and manage
const shop_list = [{
        id: 0, // shop id
        title: "Bacon Stand", // shop name
        image: "bacon", // shop image name
        time_frame: 1000, // amount of time it takes the shop to produce revenue every cycle
        revenue: 1 // amount of revenue the shop produces every cycle
    },
    {
        id: 1,
        title: "Cat Groomer",
        image: "cat",
        time_frame: 2000,
        revenue: 10
    },
    {
        id: 2,
        title: "Clown Rental",
        image: "clown",
        time_frame: 4000,
        revenue: 100
    },
    {
        id: 3,
        title: "Game Arcade",
        image: "arcade",
        time_frame: 8000,
        revenue: 1000
    },
    {
        id: 4,
        title: "Ghost Buster",
        image: "car",
        time_frame: 16000,
        revenue: 10000
    },
    {
        id: 5,
        title: "Soap Bakery",
        image: "soap",
        time_frame: 32000,
        revenue: 100000
    },
    {
        id: 6,
        title: "Coworking Space",
        image: "coworking",
        time_frame: 64000,
        revenue: 1000000
    },
    {
        id: 7,
        title: "Cell Tower",
        image: "cell",
        time_frame: 128000,
        revenue: 10000000
    },
    {
        id: 8,
        title: "Cloud Storage",
        image: "cloud",
        time_frame: 256000,
        revenue: 100000000
    },
    {
        id: 9,
        title: "Spy Satellite",
        image: "satellite",
        time_frame: 512000,
        revenue: 1000000000
    }
];

// These are actual instances of the shops that the user owns - this data is personal to each user
let inventory_list = [{
	id: 0, // shop id
	level: 1, // the quantity of this type of shop that the user owns
	time_stamp: 0, // the start time for the current revenue cycle of this shop - user must press Start or Manage
	is_managed: false // has the user paid to unlock a manager for this shop? If so, the revenue cycle will be started automatically
},
{
	id: 1,
	level: 0,
	time_stamp: 0,
	is_managed: false
},
{
	id: 2,
	level: 0,
	time_stamp: 0,
	is_managed: false
},
{
	id: 3,
	level: 0,
	time_stamp: 0,
	is_managed: false
},
{
	id: 4,
	level: 0,
	time_stamp: 0,
	is_managed: false
},
{
	id: 5,
	level: 0,
	time_stamp: 0,
	is_managed: false
},
{
	id: 6,
	level: 0,
	time_stamp: 0,
	is_managed: false
},
{
	id: 7,
	level: 0,
	time_stamp: 0,
	is_managed: false
},
{
	id: 8,
	level: 0,
	time_stamp: 0,
	is_managed: false
},
{
	id: 9,
	level: 0,
	time_stamp: 0,
	is_managed: false
}]